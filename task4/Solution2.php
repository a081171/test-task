<?php

class Solution2
{
    use Valid;

    /**
     * Через строки
     *
     * @param string $a
     * @param string $b
     *
     * @return string
     */
    public static function sum(string $a, string $b): string {
        self::validate($a);
        self::validate($b);

        $a        = strrev($a);
        $b        = strrev($b);
        $i        = 0;
        $length   = strlen($b);
        $remember = 0;

        while ($i < $length || $remember > 0) {
            $simpleSumm = (int) ($a[$i] ?? 0) + (int) ($b[$i] ?? 0) + $remember;
            $a[$i]      = $simpleSumm % 10;
            $remember   = (int) ($simpleSumm / 10);
            $i++;
        }

        return strrev($a);
    }
}
